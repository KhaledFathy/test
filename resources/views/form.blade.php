<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>

<form role="form" class="form-horizontal"  action="{{route('submit')}}" method="post">
    {!! csrf_field() !!}
    <div class="input-group">
    <span class="input-group-addon" id="basic-addon1">@</span>
        <input type="text" class="form-control" name = "name" placeholder="Product Name" required/>
</div>

<div class="input-group">
    <span class="input-group-addon" id="basic-addon2">@</span>
    <input type="text" class="form-control" name = "amount" placeholder="Amount in Stock" required/>
</div>


<div class="input-group">
    <span class="input-group-addon" id="basic-addon3">@</span>
    <input type="text" class="form-control" name = "price" placeholder="Price Per unit" required/>
</div>
    

    <input id="date" name="date" style="display: none;">

    <input type="submit" class="btn btn-info" value="Submit">
</form>



<tr>
    <th>Product Name</th>
    <th>Quantity in Stock</th>
    <th>Price Per Item</th>
    <th>Datetime Submitted</th>
    <th>Total Value Number</th>
</tr>


@foreach($data as $row)
    <tr>
        <td>{{$row['name']}}</td>
        <td>{{$row['amount']}}</td>
        <td>{{$row['price']}}</td>
        <td>{{$row['date']}}</td>
        <td>{{$row['amount']*$row['price']}}</td>

    </tr>
@endforeach


<script type="text/javascript">
    document.getElementById('date').value = Date();
</script>

</body>
</html>
