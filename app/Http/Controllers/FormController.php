<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use Illuminate\Http\Response;

class FormController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        $file = 'database.txt';
        $data = json_decode(file_get_contents($file),TRUE);
        return view('form')->with('data',$data);
    }

    public function submit(Request $data)
    {
        $file = 'database.txt';
        file_put_contents($file, json_encode($data->all(),TRUE));
        $data = json_decode(file_get_contents($file),TRUE);
        return view('form')->with('data',$data);
    }
}